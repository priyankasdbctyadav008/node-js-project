let users = [];
//user join room using login and select user.
function joinUser(socketId, loginUser, selectUser) {
    const user = {
        socketID: socketId,
        loginUser: loginUser,
        selectUser: selectUser
    }
    users.push(user);
    return user;
}
//findUser find the iser using id.
function findUser(id) {
    return users.find((user) => {
        user.id === id;
    })
}

function removeUser(id) {
    const getID = users => users.socketID === id;
    const index = users.findIndex(getID);
    if (index !== -1) {
        return users.splice(index, 1)[0];
    }
}

module.exports = { joinUser, removeUser, findUser }