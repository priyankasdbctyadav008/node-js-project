const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require("path");
const routes = require('./js/registration');
const ChatList = require('./models/chatSchema');
const cors = require('cors');
const url = require('url');
var moment = require('moment');

const port = process.env.port || 5000;

app.use(cors());
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, "views")));
app.use(express.static(path.join(__dirname, "/public")));
app.use(express.static(path.join(__dirname, "/js")));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use('/', routes);

app.get('/', (req, res) => {
    return res.redirect('index.html');
});

let users = [];
function joinUser(socketId, loginUser, selectUser) {
    const user = {
        socketID: socketId,
        loginUser: loginUser,
        selectUser: selectUser
    }
    users.push(user);
    return user;
}

function removeUser(id) {
    const getID = users => users.socketID === id;
    const index = users.findIndex(getID);
    if (index !== -1) {
        return users.splice(index, 1)[0];
    }
}

io.on("connection", function (socket) {

    console.log('connected');

    // room 
    socket.on("join room", (data) => {

        let Newuser = joinUser(socket.id, data.loginUser, data.selectUser);
        console.log({ users });
        socket.emit('send data', { id: socket.id, loginUser: Newuser.loginUser, selectUser: Newuser.selectUser });
        socket.join(data.loginUser);
        console.log(data.loginUser);
    });

    // new user join message
    socket.on('new-user-joined', (data) => {
        console.log({ data });
        socket.broadcast.emit('user-joined', data);
    });

    // chat message save with collection
    socket.on("chat message", (data) => {
        const saveCollection = new ChatList({
            socketId: socket.id,
            loginUser: data.loginUser,
            selectUser: data.selectUser,
            userMessage: data.message
        });
        saveCollection.save();
        socket.broadcast.emit("receive", { data: data, id: socket.id });
    });

    //find message.
    ChatList.find({}, (err, oldMessages) => {
        console.log('get messages', oldMessages);
        socket.emit("get-old-messages", oldMessages);
    });

    // user left message.
    socket.on("disconnect", () => {
        const user = removeUser(socket.id);
        socket.broadcast.emit('left', user);
        console.log({ users });
    });
});

//port 
http.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
});





