jQuery(document).ready(function () {
    jQuery("#loginForm").validate({
        rules: {

            email: {

                required: true,
                email: true
            },
            password: {

                required: true,
            }
        },
        //error message for jquery.
        messages: {

            email: {

                required: "Please Enter your email",
                email: "Please Enter a valid email address"
            },
            password: {

                required: "Please Enter a Password",
            }
        }
    })
});