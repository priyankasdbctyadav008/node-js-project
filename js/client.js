const socket = io();

const form = document.getElementById('send-container');
const messageInput = document.getElementById('messageInp');
const messageContainer = document.querySelector(".container");

const append = (message, position) => {
    const messageElement = document.createElement('div');
    // const p = document.createElement('p');
    // p.innerText = moment().format("hh:mm");
    messageElement.innerText = message;
    messageElement.classList.add('message');
    messageElement.classList.add(position);
    // messageElement.append(p);
    messageContainer.append(messageElement);
}

const urlParams = new URLSearchParams(window.location.search);
const selectUser = urlParams.get('to');
console.log({ selectUser });

//user name stored in session .
const loginUser = sessionStorage.getItem('Username');
console.log({ loginUser });

var connectedUsers = { loginUser: loginUser, selectUser: selectUser };

var My_Socket_ID = '';

socket.emit("join room", connectedUsers);

socket.on('send data', (data) => {

    My_Socket_ID = data.id;
    console.log(" My Socket ID:" + My_Socket_ID);
});

socket.emit('new-user-joined', connectedUsers);

// user join
socket.on('user-joined', (data) => {
    console.log('user joined', data);
    if (data.loginUser == selectUser && data.selectUser == loginUser) {
        append(`${selectUser} joined the chat`, 'left');
    }
});


document.getElementsByTagName("form")[0]
    .addEventListener('submit', (e) => {
        e.preventDefault();
        const message = messageInput.value;

        append(`You: ${message}`, 'right');
        socket.emit("chat message", { loginUser: loginUser, selectUser: selectUser, message: message, id: My_Socket_ID });
        messageInput.value = '';
    });

//chat message to receice login ang select user 
socket.on('receive', data => {
    console.log('chat-message-receive', { data });
    if (data.data.loginUser == selectUser && data.data.selectUser == loginUser) {
        append(`${data.data.loginUser}: ${data.data.message}`, 'left');
    }
    else {
        console.log(`${data.data.loginUser}: ${data.data.message}`);
    }
});

// get old message.
socket.on('get-old-messages', (data) => {
    console.log('old message', data);
    data.forEach(message => {
        if (message.loginUser == loginUser && message.selectUser == selectUser) {
            append(`You: ${message.userMessage}`, 'right');
        }
        else {
            append(`${message.loginUser}: ${message.userMessage}`, 'left');
        }
    });
});

//message show in chat window.
socket.on('left', (user) => {
    console.log('left', user);
    if (user.loginUser == selectUser && user.selectUser == loginUser) {
        append(`${user.loginUser} left the chat`, 'left');
    }
});
