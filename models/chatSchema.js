const mongoose = require('mongoose');
const mongooseDB = require('./db');

const chatSchema = new mongoose.Schema({
    socketId: String,
    loginUser: String,
    selectUser: String,
    userMessage: {
        type: String
    }
}, { timestamps: true });

//chat list is your collectin name.

const ChatList = new mongoose.model("ChatList", chatSchema);
module.exports = ChatList;
console.log("ChatList called");