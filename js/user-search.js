let searchBox = document.querySelector(".searchBox");
let filterSearch, ulItem, liItem, links;
function processResults() {
    console.log("processResult");
    filterSearch = searchBox.value.toUpperCase();
    console.log("filterSearch", filterSearch);
    ulItem = document.querySelector(".paginationuser");
    console.log("Ul Items", ulItem);
    liItem = ulItem.getElementsByTagName("li");
    console.log("Li Items", liItem);
    Array.from(liItem).forEach(element => {
        links = element.getElementsByTagName("a")[0];
        console.log("Element name", links);
        if (links.innerHTML.toUpperCase().indexOf(filterSearch) > -1) {
            element.style.display = "";
        } else {
            element.style.display = "none";
        }
    });
}
searchBox.addEventListener("keyup", () => {
    processResults();
});