$(document).ready(function () {
    $("#registrationForm").on("submit", function (e) {
        e.preventDefault();
        console.log("click button");
        var form = $(this);

        $.ajax({

            type: form.attr('method'),
            url: "/registration",
            data: form.serialize()
        }).done(function (data) {
            console.log(data.result);
            // alert("Registration Successfull");
            Swal.fire({

                position: 'center',
                icon: 'success',
                text: "The User Successfully Register",
                title: ' User Registeration Successfully',
                showConfirmButton: false,
                timer: 200000000
            });
            window.location.href = 'login.html';

        }).fail(function (data) {

            console.log(data);
            var data = jQuery.parseJSON(data.responseText);

            $(".err").html(data.msg);
            console.log(data.msg);

            for (var i = 0; i < data.errors.length; i++) {

                $('#' + data.errors[i].param).next().html('<span style="color:red">' + data.errors[i].msg + '</span>');

            };
        });
    });
});
// document.querySelector(".registerbtn").addEventListener("click", function () {
//     Swal.fire({
//         title: "Alert Set on Timer",
//         text: "This alert will disappear after 3 seconds.",
//         position: "bottom",
//         backdrop: "linear-gradient(yellow, orange)",
//         background: "white",
//         allowOutsideClick: false,
//         allowEscapeKey: false,
//         allowEnterKey: false,
//         showConfirmButton: false,
//         showCancelButton: false,
//         timer: 3000
//     });
// });

