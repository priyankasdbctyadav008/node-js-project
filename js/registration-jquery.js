jQuery(document).ready(function ($) {
    jQuery("#registrationForm").validate({
        rules: {
            username: {
                required: true,
                minlength: 2,
            },

            password: {
                required: true,
                minlength: 6
            },

            email: {
                required: true,
                email: true
            },
        },
        //error message for jquery.
        messages: {

            username: {

                required: "please enter your name jquery",
                minlength: "your username must consist of at least 2 characters "
            },

            password: {
                required: "please provide a password jquery",
                minlength: "your password must be 6 characters "
            },

            email: {
                required: "please enter a valid email address jquery"
            }

        },
    });
});

//function to show and hide password.
function myFunction() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}