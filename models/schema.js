
const mongoose = require('mongoose');
const mongooseDB = require('./db');



const schema = new mongoose.Schema({

    username: {
        type: String,
        required: true,
        minlength: [2, "Name minimum have 2 words"],
    },

    email: {
        type: String,
        trim: true,
        unique: true,
        required: "Email is required",
    },

    password: {
        type: String,
        require: true,
        minlength: [6, "Password minimum length is 6"],
    },

    resetPasswordToken: String,
    resetPasswordExpires: Date

});

const UserList = new mongoose.model("UserList", schema);
module.exports = UserList;
console.log("schema called");