$(document).ready(function () {
    $("#loginForm").on("submit", function (e) {

        e.preventDefault();
        console.log("click button");
        var form = $(this);

        $.ajax({
            type: form.attr('method'),
            url: 'http://localhost:5000/login',
            data: form.serialize()
        }).done(function (data) {

            localStorage.setItem("EmailToken", data.token);

            sessionStorage.setItem("Username", data.userMap[0].username);

            localStorage.setItem("Email", data.userMap[0].email);


            console.log(data);

            // alert("Login Done");
            Swal.fire({

                position: 'center',
                icon: 'success',
                text: "The User Successfully Login",
                title: ' User Login Successfully',
                showConfirmButton: false,
                timer: 200000000
            });

            window.location.href = "/user-list?token=" + localStorage.getItem('EmailToken');

            window.localStorage.clear();

        }).fail(function (data) {
            console.log(data);
            var resData = JSON.parse(data.responseText);
            console.log(resData);

            if (data.status == 400) {
                $('#invalid_msg').html(resData.message);
            }

            for (var i = 0; i < resData.errors.length; i++) {
                $('#' + resData.errors[i].param).next().html('<span style="color:red">' + resData.errors[i].msg + '</span>');
            }
        });
    });
});